﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Client_Server
{
    public class Server
    {
        private string Answer = "";
        public Server(string answer)
        {
            Answer = answer;
        }
        public string getAnswer()
        {
            return Answer;
        }
    }
    public class Client
    {
        private string AnswerFromServer;
        public string Connect(Server s)
        {
            AnswerFromServer = s.getAnswer();
            return AnswerFromServer;
        }
        public void printAnswer()
        {
            Console.WriteLine(AnswerFromServer);
        }
    }
    [TestFixture]
    public class Tester
    {
        [TestCase]

        public void Test()
        {
            Server S = new Server("OK");
            Client C = new Client();
            //string result = C.Connect(S);
            //string 
            Assert.IsTrue(C.Connect(S)==S.getAnswer());
        }

    }
    class Program
    {
        static void Main(string[] args)
        {
           
            

        }
    }
}
